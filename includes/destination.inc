<?php

/**
 * @file
 * Class to handle the Cloud Files backup destination.
 */

/**
 * A destination for sending database backups on a Cloud Files server.
 */
class backup_migrate_cloudfiles_destination extends backup_migrate_destination_remote {
  var $supported_ops = array('scheduled backup', 'manual backup', 'restore', 'list files', 'configure', 'delete');
  var $cf = NULL;

  /**
   * Save to the Cloud Files destination.
   */
  function save_file($file, $settings) {
    $cf = $this->cf_object();
    if ($cf->create_object($file->filename())->load_from_filename($file->filepath())) {
      return $file;
    }
    return FALSE;
  }

  /**
   * Load from the Cloud Files destination.
   */
  function load_file($file_id) {
    backup_migrate_include('files');
    $file = new backup_file(array('filename' => $file_id));
    $cf = $this->cf_object();
    if ($cf->get_object($file_id)->save_to_filename($file->filepath())) {
      return $file;
    }
    return NULL;
  }

  /**
   * Delete from the Cloud Files destination.
   */
  function delete_file($file_id) {
    $cf = $this->cf_object();
    $cf->delete_object($file_id);
  }

  /**
   * List all files from the Cloud Files destination.
   */
  function list_files() {
    backup_migrate_include('files');
    $files = array();
    $cf = $this->cf_object();
    foreach ($cf->get_objects() as $obj) {
      $info = array(
        'filename' => $obj->name,
        'filesize' => $obj->content_length,
        'filetime' => strtotime($obj->last_modified),
      );

      $files[$obj->name] = new backup_file($info);
    }
    return $files;
  }

  /**
   * Get the form the settings for this destination.
   */
  function edit_form() {
    $form = parent::edit_form();
    $form['scheme']['#type'] = 'value';
    $form['scheme']['#value'] = 'https';
    $form['host']['#type'] = 'value';
    $form['host']['#value'] = 'api.rackspacecloud.com';
    $form['path']['#title'] = t('Cloud Files Container');
    $form['user']['#title'] = t('Username');
    $form['pass']['#title'] = t('API Key');
    return $form;
  }

  function cf_object() {
    if (is_null($this->cf)) {
      if ($path = libraries_get_path('cloudfiles')) {
        require_once($path . '/cloudfiles.php');
      }

      $auth = new CF_Authentication($this->dest_url['user'], $this->dest_url['pass']);
      $auth->authenticate();

      $conn = new CF_Connection($auth);

      $this->cf = $conn->create_container($this->dest_url['path']);
    }

    return $this->cf;
  }
}


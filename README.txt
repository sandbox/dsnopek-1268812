Backup and Migrate: Cloud Files
===============================

Allows the fine [Backup and Migrate](http://drupal.org/project/backup_migrate) module to backup to
[Rackspace Cloud Files](http://www.rackspace.com/cloud/cloud_hosting_products/files/) (Rackspace's
alternative to [Amazon S3](http://aws.amazon.com/s3/).  This is very useful you host your site on
Rackspace Cloud instead of Amazon EC2, because (1) it will be faster and (2) you won't be charged
for bandwidth between your server and Cloud Files.

Installation
------------

 1. Download and place the 'backup_migrate_cloudfiles' module into your sites/all/modules directory

 2. Download the Cloud Files PHP API from Rackspace's account on GitHub:

    a. Go to https://github.com/rackspace/php-cloudfiles

	b. Click "Downloads" and select .tar.gz or .zip (which ever you know how to extract!)

	c. Save to disk!

 3. Extract the file you download into sites/all/libraries and rename the directory: "cloudfiles"

 4. Enable the 'backup_migrate_cloudfiles' module in your Drupal installing, either:

      http://YOUR_SITE_NAME_HERE/admin/build/modules

	    - or, via drush -
	
	  drush en backup_migrate_cloudfiles

 5. Now you can create a destionation of type "Rackspace Cloud Files Container" here:

      http://YOUR_SITE_NAME_HERE/admin/content/backup_migrate/destination/add

Copyright
---------

Copyright (C) 2011 David Snopek

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

